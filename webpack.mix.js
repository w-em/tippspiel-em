const mix = require('laravel-mix');
const webpack = require('webpack')
const glob = require("glob");
const path = require('path');
require('laravel-mix-polyfill');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");

const uniqueFileNames = (pathArray , ext) => {
    let usedNames = [];
    let iconArray = [];
    for (let iconPath of pathArray.reverse ()) {
        const iconName = path.basename (iconPath, ext);
        if (!usedNames.includes (iconName)) {
            usedNames.push (iconName);
            iconArray.push (iconPath);
        }
    }
    return iconArray;
};

mix.webpackConfig({
    optimization: {
        splitChunks: false
    },
    plugins: [
        new MiniCssExtractPlugin(),
        new SVGSpritemapPlugin(
            uniqueFileNames ([
                //Sort the icons by importance! (the last line overwrites all previous ones, if the name equals)
                ...glob.sync ('./node_modules/@cake-hub/lidl-web-bootstrap_theme/assets/icons/!(_)**/!(_)*.svg'),
                ...glob.sync (path.resolve(__dirname, 'resources/assets/icons/!(_)*.svg')),
                ...glob.sync (path.resolve(__dirname, 'resources/assets/icons/!(_)**/!(_)*.svg'))
            ], '.svg'), {
                output: {
                    filename: "images/icon__sprite.svg",
                    svgo: true,
                    chunk: { keep: true }
                },
                sprite: {
                    prefix: false,
                }
            }
        ),
        new FixStyleOnlyEntriesPlugin ()
    ],
    resolve: {
        alias: {
            src: path.resolve(__dirname, './resources/js'),
            public: path.resolve(__dirname, '../public/'),
            '@': path.resolve(__dirname, './resources/js')
        }
    }
}).options({
    extractVueStyles: false,
    processCssUrls: true,
    terser: {},
    purifyCss: false,
    postCss: [
        require('postcss-inline-svg')({
            removeFill: true,
            xmlns: true,
            path: path.resolve(__dirname, '../../')
        }),
        require('autoprefixer')({
            overrideBrowserslist: [
                "> 0.1%",
                "last 7 versions",
                "Android >= 4",
                "Firefox >= 20",
                "iOS >= 8"
            ],
            flexbox: true,
        })
    ],
    clearConsole: false,
    cssNano: {
        discardComments: {
            removeAll: true
        },
    }
})

// mix.copyDirectory('resources/images', 'public/images');
mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/backend.js', 'public/js')
    .sass('resources/scss/app.scss', 'public/css')
    .sass('resources/scss/backend.scss', 'public/css')

if (mix.inProduction()) {
    mix.version();
}
else {
    // Enable sourcemaps
    mix.sourceMaps();
    mix.browserSync({
        proxy: 'localhost'
    });
}

