<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_schedule', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('league_id')->index();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('name')->index()->nullable();
            $table->string('short_name')->index()->nullable();
            $table->boolean('show_groups');
            $table->timestamps();
            $table->softDeletes();

            $table->index(['league_id', 'start_date', 'end_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_schedule');
    }
}
