<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('league_id')->index();
            $table->unsignedInteger('game_schedule_id')->index();

            $table->tinyInteger('type', false, true)->default(0);

            $table->dateTime('start_date')->nullable()->index();
            $table->dateTime('max_tipp_date')->nullable(); // bis wann darf getippt werden

            $table->text('question')->nullable();
            $table->text('answer')->nullable();
            $table->tinyInteger('point_rule', false, true)->default(0);

            $table->index(['league_id', 'game_schedule_id']);
            $table->index(['league_id', 'game_schedule_id', 'type']);
            $table->index(['league_id', 'game_schedule_id', 'type', 'start_date']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
