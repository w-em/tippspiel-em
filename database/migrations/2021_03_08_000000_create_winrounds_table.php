<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('win_rounds', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('league_id')->index();
            $table->string('name');
            $table->unsignedInteger('type')->default(0)->index();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('win_round_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('win_round_id')->index();
            $table->unsignedInteger('value')->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('win_rounds');
        Schema::dropIfExists('win_round_questions');
    }
}
