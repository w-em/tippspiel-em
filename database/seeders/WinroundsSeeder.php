<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WinroundsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ["1. Spieltag", "1,2,3,4,5,6,7"],
            ["2. Spieltag", "8,9,10,11,12"],
            ["3. Spieltag", "13,14,15,16,17,18"],
            ["4. Spieltag", "19,20,21,22,23,24"],
            ["5. Spieltag", "25,26,27,28,29,30"],
            ["6. Spieltag", "31,32,33,34,35,36"],
            ["Achtelfinale", "37,38,39,40,41,42,43,44"],
            ["Viertelfinale", "45,46,47,48"],
            ["Halbfinale", "49,50"],
            ["Finale", "51"],
            ["Bonus", "52,53,54,55,56,57,58,59"],
        ];

        $allQuestionIds = [];
        $questions = \App\Models\Question::get();
        foreach($questions as $question) {
            $allQuestionIds[] = $question->id;
        }

        /*
        foreach ($datas as $record) {

            $questionIds = explode(",", $record[1]);
            $winRound = \App\Models\WinRound::create([
                "type" => 0,
                "league_id" => 1,
                "name" => $record[0]
            ]);

            foreach ($questionIds as $questionId) {
                $allQuestionIds[] = (int) $questionId;
                $winRoundQuestion = \App\Models\WinRoundQuestion::create([
                    "win_round_id" => $winRound->id,
                    "value" => (int) $questionId
                ]);
            }
        }
        */
        $winRound = \App\Models\WinRound::create([
            "type" => 0,
            "league_id" => 1,
            "name" => 'Gesamtgewinner'
        ]);

        foreach ($allQuestionIds as $questionId) {
            $winRoundQuestion = \App\Models\WinRoundQuestion::create([
                "win_round_id" => $winRound->id,
                "value" => (int) $questionId
            ]);
        }

    }
}
