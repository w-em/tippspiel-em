<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $users = ['sj@w-em.com', 'Fabian.Keitel@lidl.at', 'Andreas.Essl@lidl.at'];

        foreach ($users as $email) {
            \App\Models\User::create([
                'name' => $email,
                'email' => $email,
                'password' => $email,
                'is_admin' => 1,
                'status' => 0,
                'email_verified_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);
        }



    }
}
