<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [1, 'Italien', 'A', 'ITA'],
            [2, 'Schweiz', 'A', 'SUI'],
            [3, 'Türkei', 'A', 'TUR'],
            [4, 'Wales', 'A', 'WAL'],

            [5, 'Belgien', 'B', 'BEL'],
            [6, 'Dänemark', 'B', 'DEN'],
            [7, 'Finnland', 'B', 'FIN'],
            [8, 'Russland', 'B', 'RUS'],

            [9,'Niederlande', 'C', 'NED'],
            [10,'Nordmazedonien', 'C', 'MKD'],
            [11,'Ukraine', 'C', 'UKR'],
            [12,'Österreich', 'C', 'AUT'],

            [13,'England', 'D', 'ENG'],
            [14,'Kroatien', 'D', 'CRO'],
            [15,'Schottland', 'D', 'SCO'],
            [16,'Tschechien', 'D', 'CZE'],

            [17,'Polen', 'E', 'POL'],
            [18,'Schweden', 'E', 'SWE'],
            [19,'Slowakei', 'E', 'SVK'],
            [20,'Spanien', 'E', 'ESP'],

            [21,'Deutschland', 'F', 'GER'],
            [22,'Frankreich', 'F', 'FRA'],
            [23,'Portugal', 'F', 'POR'],
            [24,'Ungarn', 'F', 'HUN'],
        ];

        foreach ($data as $record) {
            $team = \App\Models\Team::create([
                'id' => $record[0],
                'name' => $record[1],
                'short_name' => $record[3],
                'league_id' => 1,
                'group' => $record[2],
            ]);
        }

    }
}
