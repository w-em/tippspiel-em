<?php

namespace Database\Seeders;

use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GameScheduleSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            ['11.06.2021', '15.06.2021', '1. Spieltag', '1.', 1],
            ['16.06.2021', '19.06.2021', '2. Spieltag', '2.', 1],
            ['20.06.2021', '23.06.2021', '3. Spieltag', '3.', 1],
            ['26.06.2021', '29.06.2021', 'Achtelfinale', 'Ac'],
            ['02.07.2021', '03.07.2021', 'Viertelfinale', 'Vi'],
            ['06.07.2021', '07.07.2021', 'Halbfinale', 'Ha'],
            ['11.07.2021', '11.07.2021', 'Finale', 'Fi'],
            ['11.06.2021', '12.07.2021', 'Bonus', 'Bo'],
        ];

        foreach ($data as $key => $record) {
            $gameDay = \App\Models\GameSchedule::create([
                'start_date' => Carbon::createFromFormat('d.m.Y', $record[0], 'Europe/London')->setHour(0)->setMinute(0)->setSecond(0),
                'end_date' => Carbon::createFromFormat('d.m.Y', $record[1], 'Europe/London')->setHour(23)->setMinute(59)->setSecond(59),
                'league_id' => 1,
                'name' => $record[2],
                'short_name' => $record[3],
                'show_groups' => key_exists(4, $record) && (int) $record[4] === 1
            ]);
        }

    }
}
