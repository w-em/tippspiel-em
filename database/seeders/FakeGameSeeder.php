<?php

namespace Database\Seeders;

use App\Models\UserAnswer;
use App\Models\GameSchedule;
use App\Models\Question;
use App\Models\User;
use App\Services\CalculateService;
use Illuminate\Database\Seeder;

class FakeGameSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(60)->create();

        //$this->updateGameDates();
        $this->randomizeGameResults();
        $this->randomizeUserBets();
        $this->calculateHighscore();
    }

    protected function calculateHighscore() {
        $questions = Question::get();
        foreach ($questions as $question) {
            CalculateService::calculateQuestion($question);
        }
    }

    protected function randomizeUserBets() {
        $users = User::get();
        $questions = Question::get();

        foreach ($users as $user) {
            foreach ($questions as $question) {
                if ($question->type === Question::TYPE_GAME) {
                    $data = rand(0,3) . ':' . rand(0,3);
                    UserAnswer::updateOrCreate(
                        [
                            'user_id' => $user->id,
                            'question_id' => $question->id
                        ],
                        [
                            'data' => $data
                        ]
                    );
                }
                if ($question->type === Question::TYPE_BONUS) {
                    // $question->answer = rand()
                }
            }
        }
    }

    protected function randomizeGameResults() {
        $questions = Question::get();

        foreach ($questions as $question) {
            if ($question->type === Question::TYPE_GAME) {
                $question->answer = rand(0,3) . ':' . rand(0,3);
            }
            if ($question->type === Question::TYPE_BONUS) {
                // $question->answer = rand()
            }

            $question->save();
        }
    }

    protected function updateGameDates() {
        $dates = Question::all();
        foreach ($dates as $date) {
            $date->start_date = $date->start_date->subYear(1);
            $date->max_tipp_date = $date->max_tipp_date->subYear(1);
            $date->save();
        }

        $dates = GameSchedule::all();
        foreach ($dates as $date) {
            $date->start_date = $date->start_date->subYear(1);
            $date->end_date = $date->end_date->subYear(1);
            $date->save();
        }
    }
}
