<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LeagueSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $league = \App\Models\League::create([
            'id' => 1,
            'name' => 'Fussball EM 2021',
            'active' => true,
            'start_date' => \Carbon\Carbon::now()->toDateTimeString(),
            'end_date' => \Carbon\Carbon::now()->addMonth(8)->toDateTimeString(),
        ]);
    }
}
