<?php

namespace Database\Seeders;

use App\Models\Question;
use Illuminate\Database\Seeder;

class LeaguePointSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [1, '2-4 Punkte', ['tendence' => 2, 'difference' => 0, 'exact' => 4], Question::TYPE_GAME],
            [2, 'Bonus Punkte', ['exact' => 10], Question::TYPE_BONUS],
        ];

        foreach ($data as $record) {
            $leaguePoints = \App\Models\PointRule::create([
                'id' => $record[0],
                'type' => $record[3],
                'name' => $record[1],
                'data' => $record[2]
            ]);
        }

    }
}
