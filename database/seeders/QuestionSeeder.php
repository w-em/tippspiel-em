<?php

namespace Database\Seeders;

use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['11.06.2021 21:00:00', 'Türkei', 'Italien', 'Gruppe A', 1],
            ['12.06.2021 15:00:00', 'Wales', 'Schweiz', 'Gruppe A', 1],
            ['12.06.2021 18:00:00', 'Dänemark', 'Finnland', 'Gruppe B', 1],
            ['12.06.2021 21:00:00', 'Belgien', 'Russland', 'Gruppe B', 1],
            ['13.06.2021 15:00:00', 'England', 'Kroatien', 'Gruppe D', 1],
            ['13.06.2021 18:00:00', 'Österreich', 'Nordmazedonien', 'Gruppe C', 1],
            ['13.06.2021 21:00:00', 'Niederlande', 'Ukraine', 'Gruppe C', 1],
            ['14.06.2021 15:00:00', 'Schottland', 'Tschechien', 'Gruppe D', 1],
            ['14.06.2021 18:00:00', 'Polen', 'Slowakei', 'Gruppe E', 1],
            ['14.06.2021 21:00:00', 'Spanien', 'Schweden', 'Gruppe E', 1],
            ['15.06.2021 18:00:00', 'Ungarn', 'Portugal', 'Gruppe F', 1],
            ['15.06.2021 21:00:00', 'Frankreich', 'Deutschland', 'Gruppe F', 1],
            ['16.06.2021 15:00:00', 'Finnland', 'Russland', 'Gruppe B', 2],
            ['16.06.2021 18:00:00', 'Türkei', 'Wales', 'Gruppe A', 2],
            ['16.06.2021 21:00:00', 'Italien', 'Schweiz', 'Gruppe A', 2],
            ['17.06.2021 15:00:00', 'Ukraine', 'Nordmazedonien', 'Gruppe C', 2],
            ['17.06.2021 18:00:00', 'Dänemark', 'Belgien', 'Gruppe B', 2],
            ['17.06.2021 21:00:00', 'Niederlande', 'Österreich', 'Gruppe C', 2],
            ['18.06.2021 15:00:00', 'Schweden', 'Slowakei', 'Gruppe E', 2],
            ['18.06.2021 18:00:00', 'Kroatien', 'Tschechien', 'Gruppe D', 2],
            ['18.06.2021 21:00:00', 'England', 'Schottland', 'Gruppe D', 2],
            ['19.06.2021 15:00:00', 'Ungarn', 'Frankreich', 'Gruppe F', 2],
            ['19.06.2021 18:00:00', 'Portugal', 'Deutschland', 'Gruppe F', 2],
            ['19.06.2021 21:00:00', 'Spanien', 'Polen', 'Gruppe E', 2],
            ['20.06.2021 18:00:00', 'Italien', 'Wales', 'Gruppe A', 3],
            ['20.06.2021 18:00:00', 'Schweiz', 'Türkei', 'Gruppe A', 3],
            ['21.06.2021 18:00:00', 'Ukraine', 'Österreich', 'Gruppe C', 3],
            ['21.06.2021 18:00:00', 'Nordmazedonien', 'Niederlande', 'Gruppe C', 3],
            ['21.06.2021 21:00:00', 'Russland', 'Dänemark', 'Gruppe B', 3],
            ['21.06.2021 21:00:00', 'Finnland', 'Belgien', 'Gruppe B', 3],
            ['22.06.2021 21:00:00', 'Kroatien', 'Schottland', 'Gruppe D', 3],
            ['22.06.2021 21:00:00', 'Tschechien', 'England', 'Gruppe D', 3],
            ['23.06.2021 18:00:00', 'Slowakei', 'Spanien', 'Gruppe E', 3],
            ['23.06.2021 18:00:00', 'Schweden', 'Polen', 'Gruppe E', 3],
            ['23.06.2021 21:00:00', 'Portugal', 'Frankreich', 'Gruppe F', 3],
            ['23.06.2021 21:00:00', 'Deutschland', 'Ungarn', 'Gruppe F', 3],
            ['26.06.2021 18:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['26.06.2021 21:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['27.06.2021 18:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['27.06.2021 21:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['28.06.2021 18:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['28.06.2021 21:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['29.06.2021 18:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['29.06.2021 21:00:00', 'unbekannt', 'unbekannt', null, 4],
            ['02.07.2021 18:00:00', 'unbekannt', 'unbekannt', null,5],
            ['02.07.2021 21:00:00', 'unbekannt', 'unbekannt', null,5],
            ['03.07.2021 18:00:00', 'unbekannt', 'unbekannt', null,5],
            ['03.07.2021 21:00:00', 'unbekannt', 'unbekannt', null,5],
            ['06.07.2021 21:00:00', 'unbekannt', 'unbekannt', null, 6],
            ['07.07.2021 21:00:00', 'unbekannt', 'unbekannt', null, 6],
            ['11.07.2021 21:00:00', 'unbekannt', 'unbekannt', null, 7],
        ];

        foreach ($data as $record) {

            $teamHomeId = null;
            $teamHome = Team::where([['name', '=', $record[1]]])->first();
            if ($teamHome) {
                $teamHomeId = $teamHome->id;
            }

            $teamGuestId = null;
            $teamGuest = Team::where([['name', '=', $record[2]]])->first();
            if ($teamHome) {
                $teamGuestId = $teamGuest->id;
            }

            $question = \App\Models\Question::create([
                'start_date' => Carbon::createFromFormat('d.m.Y H:i:s', $record[0], 'Europe/London'),
                'max_tipp_date' => Carbon::createFromFormat('d.m.Y H:i:s', $record[0], 'Europe/London')->subMinutes(1),
                'league_id' => 1,
                'game_schedule_id' => $record[4],
                'type' => 0,
                'question' => $teamHomeId . ':' . $teamGuestId,
                'point_rule' => 1
            ]);
        }

        $datas = [
            ['11.06.2021 21:00:00', 'Welche Mannschaft stellt den Spieler mit den meisten Toren?', 'all'],
            // ['11.06.2021 21:00:00', 'Wer gewinnt die Gruppe A?', ['Italien', 'Schweiz', 'Türkei', 'Wales']],
            // ['11.06.2021 21:00:00', 'Wer gewinnt die Gruppe B?', ['Belgien', 'Dänemark', 'Finnland', 'Russland']],
            // ['11.06.2021 21:00:00', 'Wer gewinnt die Gruppe C?', ['Niederlande', 'Nordmazedonien', 'Ukraine', 'Österreich']],
            // ['11.06.2021 21:00:00', 'Wer gewinnt die Gruppe D?', ['England', 'Kroatien', 'Schottland', 'Tschechien']],
            // ['11.06.2021 21:00:00', 'Wer gewinnt die Gruppe E?', ['Polen', 'Schweden', 'Slowakei', 'Spanien']],
            // ['11.06.2021 21:00:00', 'Wer gewinnt die Gruppe F?', ['Deutschland', 'Frankreich', 'Portugal', 'Ungarn']],
            ['11.06.2021 21:00:00', 'Wer wird Europameister?', 'all'],
        ];

        foreach ($datas as $record) {
            $teamIds = [];
            if (is_array($record[2])) {
                foreach ($record[2] as $teamName) {
                    $team = Team::where([['name', '=', $teamName]])->first();
                    $teamIds[] = $team->id;
                }
            } elseif ($record[2] === 'all') {
                foreach (Team::get() as $team) {
                    $teamIds[] = $team->id;
                }
            }

            $question = \App\Models\Question::create([
                'start_date' => Carbon::createFromFormat('d.m.Y H:i:s', $record[0], 'Europe/London'),
                'max_tipp_date' => Carbon::createFromFormat('d.m.Y H:i:s', $record[0], 'Europe/London')->subMinutes(1),
                'league_id' => 1,
                'game_schedule_id' => 8,
                'type' => 1,
                'question' => $record[1] . '#' . implode(',', $teamIds),
                'point_rule' => 2
            ]);
        }

    }
}
