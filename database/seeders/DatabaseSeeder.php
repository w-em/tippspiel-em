<?php

namespace Database\Seeders;

use App\Models\PointRule;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(LeagueSeeder::class);
        $this->call(LeaguePointSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(GameScheduleSeeder::class);

        $this->call(QuestionSeeder::class);
        $this->call(WinroundsSeeder::class);

        // $this->call(FakeGameSeeder::class);
    }
}
