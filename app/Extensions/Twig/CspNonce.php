<?php
namespace App\Extensions\Twig;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CspNonce extends AbstractExtension {

    public function __construct()
    {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('csp_nonce', [$this, 'cspNonce'], [
                'is_safe'           => ['html'],
                'needs_context'     => false,
                'needs_environment' => false
            ]),
        ];
    }

    public function getName()
    {
        return 'App_Extension_Twig_CspNonce';
    }

    public function cspNonce () {
        return app('csp-nonce');
    }
}
