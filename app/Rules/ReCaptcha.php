<?php

namespace App\Rules;

use App\Models\Planet\Planet;
use App\Models\Store;
use App\Models\System\SystemItem;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class ReCaptcha implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $data = [];

        // for testing no repcatcha test
        if (in_array(\Config::get('app.env'), ['testing'])) {
            return true;
        }

        $client = new Client($data);

        $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify',
            [
                'form_params'=>
                [
                    'secret'=> Config::get('recaptcha.secret'),
                    'response'=> $value
                ]
            ]);

        $body = json_decode((string)$response->getBody());
        return $body->success;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Recaptcha ungültig';
    }
}
