<?php

namespace App\Http\Controllers;

use App\Models\League;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as IlluminateController;
use Illuminate\Support\Facades\Auth;

class BaseController extends IlluminateController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $menu;

    protected $currentLeague = null;

    protected $currentRouteName = null;

    protected $defaultPaginationPerPage = 25;

    protected $user = null;

    function __construct(Request $request)
    {
        $this->currentUser = Auth::user();
        $this->currentLeague = League::findOrFail(1);
        $this->currentRouteName = \Request::route()->getName();

        view()->share('currentRouteName', $this->currentRouteName);
    }
}
