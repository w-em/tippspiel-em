<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\GameSchedule;

class GameScheduleController extends BaseController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function index () {

        $schedules = GameSchedule::where([
            ['league_id', '=', $this->currentLeague->id]
        ])->orderBy('start_date', 'asc')->get();

        foreach ($schedules as $key => $gameSchedule) {
            $games = Question::where([
                ['league_id', '=', $this->currentLeague->id],
                ['game_schedule_id', '=', $gameSchedule->id],
            ])->orderBy('start_date')->get();

            $schedules[$key]['type'] = Question::TYPE_GAME;
            $schedules[$key]['games'] = $games;

            if ($games->count() > 0) {
                if ($games->first()->type === Question::TYPE_GAME) {
                    $schedules[$key]['type'] = Question::TYPE_GAME;
                }
                if ($games->first()->type === Question::TYPE_BONUS) {
                    $schedules[$key]['type'] = Question::TYPE_BONUS;
                }
            }
        }

        return view('game-schedule.index', [
            'schedules' => $schedules,
        ]);
    }
}
