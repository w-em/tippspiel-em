<?php

namespace App\Http\Controllers;

use App\Models\Question;
use App\Models\User;
use App\Services\CalculateService;
use Illuminate\Support\Facades\Lang;

class TestController extends BaseController {

    public function index() {

        exit;
        //print_r(Lang::get('You are receiving this email because we received a password reset request for your account.'));

        $time_start = microtime(true);
        $questions = Question::whereNotNull('answer')->get();
        $question = Question::findOrFail(1);
        // foreach($questions as $question) {
            CalculateService::calculateQuestion($question);
        //}

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo '<b>Total Execution Time:</b> '.($execution_time).'Seconds';

        // 0.267
    }
}
