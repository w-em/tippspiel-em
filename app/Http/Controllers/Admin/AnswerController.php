<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\UserAnswer\EditRequest;
use App\Http\Requests\Admin\UserAnswer\StoreRequest;
use App\Http\Requests\Admin\UserAnswer\UpdateRequest;
use App\Jobs\ProcessQuestionCalculateJob;
use App\Models\PointRule;
use App\Models\Question;
use App\Models\Team;
use App\Services\CalculateService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AnswerController extends AdminController
{
    /**
     * Questions
     *
     * here all Questions
     */
    public function index(Request $request) {
        $questions = Question::orderBy('type')->orderBy('start_date')->paginate($this->defaultPaginationPerPage);
        $questions::defaultView('pagination::bootstrap-4');
        return view('admin.answers.index', [
            'questions' => $questions
        ]);
    }

    /**
     * @param EditRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(EditRequest $request, $id) {
        $question = Question::findOrFail($id);
        $pointRules = PointRule::get();
        return view('admin.answers.edit', [
            'type' => $question->type,
            'old' => $request->old(),
            'answer' => $question,
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function update(UpdateRequest $request) {

        $validated = $request->validated();
        $question = Question::findOrFail((int) $validated['id']);

        if ($question->type === Question::TYPE_GAME) {
            $ex = array_map("intval", explode(':', $validated['answer']));

            if (count($ex) !== 2 || !is_numeric($ex[0]) || !is_numeric($ex[1])) {
                throw ValidationException::withMessages([
                    'answer' => 'Ungültige Antwort',
                ]);
            }

            $question->update([
                'answer' => implode(':', $ex)
            ]);
        }

        if ($question->type === Question::TYPE_BONUS) {
            $answerId = (int) $validated['answer'];

            if (!is_numeric($answerId) || !in_array($answerId, $question->bonusSelectables->pluck('id')->toArray())) {
                throw ValidationException::withMessages([
                    'answer' => 'Ungültige Antwort',
                ]);
            }

            $question->update([
                'answer' => $answerId
            ]);
        }

        dispatch(new ProcessQuestionCalculateJob(Question::find((int) $validated['id'])));
        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.answers.edit', array('id' => $validated['id'])));

    }
}
