<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Schedule\CreateRequest;
use App\Http\Requests\Admin\Schedule\EditRequest;
use App\Http\Requests\Admin\Schedule\StoreRequest;
use App\Http\Requests\Admin\Schedule\UpdateRequest;
use App\Http\Requests\Admin\User\DeleteRequest;
use App\Models\GameSchedule;
use App\Models\Question;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends AdminController
{
    /**
     * Schedule
     *
     * here all Schedule
     */
    public function index(Request $request) {
        $gameSchedules = GameSchedule::orderBy('end_date')->paginate($this->defaultPaginationPerPage);
        $gameSchedules::defaultView('pagination::bootstrap-4');

        return view('admin.schedules.index', [
            'game_schedules' => $gameSchedules
        ]);
    }

    /**
     * show the form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(CreateRequest $request) {
        return view('admin.schedules.create', [
            'old' => $request->old(),
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(StoreRequest $request) {
        $validated = $request->validated();

        GameSchedule::create([
            'name' => $validated['name'],
            'short_name' => $validated['short_name'],
            'start_date' => $validated['start_date'],
            'end_date' => $validated['end_date'],
            'show_groups' => key_exists('show_groups', $validated) && (int) $validated['show_groups'] === 1 ? 1 : 0,
            'league_id' => $this->currentLeague->id,

        ]);

        $request->session()->flash('status', 'created-success');

        return redirect()->intended(route('admin.schedules.index'));
    }

    /**
     * @param EditRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(EditRequest $request, $id) {
        $schedule = GameSchedule::findOrFail($id);
        return view('admin.schedules.edit', [
            'schedule' => $schedule,
            'old' => $request->old(),
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(UpdateRequest $request, $id) {
        $validated = $request->validated();
        $schedule = GameSchedule::findOrFail($id);

        $schedule->update([
            'name' => $validated['name'],
            'short_name' => $validated['short_name'],
            'start_date' => $validated['start_date'],
            'end_date' => $validated['end_date'],
            'show_groups' => key_exists('show_groups', $validated) && (int) $validated['show_groups'] === 1 ? 1 : 0,
        ]);

        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.schedules.edit', array('id' => $validated['id'])));
    }

    public function delete(Request $request, $id) {
        $data = GameSchedule::findOrFail($id);

        $gameCount = Question::where([
            ['game_schedule_id', '=', $id]
        ])->count();

        return view('admin.schedules.delete', [
            'gameCount' => $gameCount,
            'data' => $data
        ]);
    }

    public function remove(DeleteRequest $request, $id)
    {
        $data = GameSchedule::findOrFail($id);

        Question::where([
            ['game_schedule_id', '=', $id]
        ])->delete();

        $data->delete();

        return redirect()->intended(route('admin.schedules.index'));

    }

}
