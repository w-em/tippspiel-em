<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\PointRule\CreateRequest;
use App\Http\Requests\Admin\PointRule\EditRequest;
use App\Http\Requests\Admin\PointRule\StoreRequest;
use App\Http\Requests\Admin\PointRule\UpdateRequest;
use App\Http\Requests\Admin\User\DeleteRequest;
use App\Jobs\ProcessQuestionCalculateJob;
use App\Models\PointRule;
use App\Models\Question;
use App\Models\Team;
use App\Services\CalculateService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PointRuleController extends AdminController
{
    /**
     * Pointrules
     */
    public function index(Request $request) {
        return view('admin.pointrules.index', [
            'rules' => PointRule::where([['league_id', '=', 1]])->get()
        ]);
    }

    /**
     * show the form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(CreateRequest $request) {
        return view('admin.pointrules.create', [
            'old' => $request->old(),
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(StoreRequest $request) {
        $validated = $request->validated();

        if ((int) $validated['type'] === Question::TYPE_GAME) {
            $data = [
                'exact' => $validated['exact'],
                'tendence' => $validated['tendence'],
                'difference' => $validated['difference'],
            ];
        }
        elseif ((int) $validated['type'] === Question::TYPE_BONUS) {
            $data = [
                'exact' => $validated['exact'],
            ];
        }

        PointRule::create([
            'name' => $validated['name'],
            'data' => $data,
            'type' => $validated['type'],
            'league_id' => 1,

        ]);

        $request->session()->flash('status', 'created-success');

        return redirect()->intended(route('admin.point_rules.index'));
    }

    /**
     * @param EditRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(EditRequest $request, $id) {
        $pointRule = PointRule::findOrFail($id);

        return view('admin.pointrules.edit', [
            'old' => $request->old(),
            'data' => $pointRule
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function update(UpdateRequest $request, $id) {

        $validated = $request->validated();

        $pointRule = PointRule::findOrFail((int) $validated['id']);

        if ((int) $validated['type'] === Question::TYPE_GAME) {
            $data = [
                'exact' => $validated['exact'],
                'tendence' => $validated['tendence'],
                'difference' => $validated['difference'],
            ];
        }
        elseif ((int) $validated['type'] === Question::TYPE_BONUS) {
            $data = [
                'exact' => $validated['exact'],
            ];
        }

        $pointRule->update([
            'name' => $validated['name'],
            'data' => $data,
            'type' => $validated['type'],
        ]);

        $questions = Question::where([['point_rule', '=', $pointRule->id]])->get();

        foreach ($questions as $question) {
            dispatch(new ProcessQuestionCalculateJob($question));
        }


        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.point_rules.edit', array('id' => $validated['id'])));

    }

    public function delete(Request $request, $id) {
        $data = PointRule::findOrFail($id);

        $questions = Question::where([
            ['point_rule', '=', $id]
        ])->count();

        return view('admin.pointrules.delete', [
            'data' => $data,
            'questionCount' => $questions
        ]);
    }


    public function remove(DeleteRequest $request, $id) {
        $pointRule = PointRule::findOrFail($id);

        $pointRule->delete();

        return redirect()->intended(route('admin.point_rules.index'));
    }
}
