<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Team\CreateRequest;
use App\Http\Requests\Admin\Team\EditRequest;
use App\Http\Requests\Admin\Team\StoreRequest;
use App\Http\Requests\Admin\Team\UpdateRequest;
use App\Http\Requests\Admin\User\DeleteRequest;
use App\Models\Question;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamController extends AdminController
{
    /**
     * Questions
     *
     * here all Questions
     */
    public function index(Request $request) {
        $teams = Team::orderBy('name')->paginate($this->defaultPaginationPerPage);
        $teams::defaultView('pagination::bootstrap-4');
        return view('admin.teams.index', [
            'teams' => $teams
        ]);
    }

    /**
     * show the form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(CreateRequest $request) {
        return view('admin.teams.create', [
            'old' => $request->old(),
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(StoreRequest $request) {
        $validated = $request->validated();

        Team::create([
            'name' => $validated['name'],
            'short_name' => $validated['short_name'],
            'league_id' => $this->currentLeague->id,
            'group' => $validated['group'],
        ]);

        $request->session()->flash('status', 'created-success');

        return redirect()->intended(route('admin.teams.index'));
    }

    /**
     * @param EditRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(EditRequest $request, $id) {
        $team = Team::findOrFail($id);
        return view('admin.teams.edit', [
            'team' => $team,
            'old' => $request->old(),
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(UpdateRequest $request, $id) {
        $validated = $request->validated();
        $team = Team::findOrFail($id);
        $team->update([
            'name' => $validated['name'],
            'short_name' => $validated['short_name'],
            'group' => $validated['group'],
        ]);

        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.teams.edit', array('id' => $validated['id'])));
    }

    public function delete(Request $request, $id) {
        $team = Team::findOrFail($id);

        $teamId = (int) $id;

        $questionCounter = $this->getCountInQuestionsIncluded($teamId);

        return view('admin.teams.delete', [
            'questionCounter' => $questionCounter,
            'team' => $team
        ]);
    }

    public function remove(DeleteRequest $request, $id) {
        $team = Team::findOrFail($id);
        $questionCounter = $this->getCountInQuestionsIncluded((int) $id);
        if ($questionCounter > 0) {
            return redirect()->intended(route('admin.teams.delete', array('id' => $id)));
        }

        $team->delete();

        return redirect()->intended(route('admin.teams.index'));
    }

    protected function getCountInQuestionsIncluded($teamId) {

        $questions = Question::get();

        $questionCounter = 0;

        foreach ($questions as $question) {
            if ($question->type === Question::TYPE_GAME) {
                if (($question->homeTeam && $question->homeTeam->id === $teamId) || ($question->guestTeam && $question->guestTeam->id === $teamId)) {
                    $questionCounter++;
                }
            } elseif ($question->type === Question::TYPE_BONUS) {
                if (in_array($teamId, $question->bonusSelectables->pluck('id')->toArray())) {
                    $questionCounter++;
                }
            }
        }

        return $questionCounter;
    }
}
