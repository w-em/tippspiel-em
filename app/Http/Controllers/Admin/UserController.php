<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\User\DeleteRequest;
use App\Http\Requests\Admin\User\StoreRequest;
use App\Http\Requests\Admin\User\UpdateRequest;
use App\Http\Requests\Admin\User\CreateRequest;
use App\Models\User;
use App\Models\UserAnswer;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends AdminController
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request) {

        $q = $request->get('q');
        if ($q) {
            $users = User::where('name','like','%' . $q . '%')->orWhere('email','like','%' . $q . '%');
            $users = $users->paginate($this->defaultPaginationPerPage);
            $users::defaultView('pagination::bootstrap-4');
            $pagination = $users->appends(['q' => $q])->render();
        } else {
            $users = User::paginate($this->defaultPaginationPerPage);
            $users::defaultView('pagination::bootstrap-4');
            $pagination = $users->render();
        }

        return view('admin.users.index', [
            'users' => $users,
            'pagination' => $pagination
        ]);
    }

    /**
     * show the form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(CreateRequest $request) {
        return view('admin.users.create', [
            'old' => $request->old()
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(StoreRequest $request) {

        $validated = $request->validated();

        $user = User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'is_admin' => $validated['is_admin'],
            'status' => $validated['status'],
            'password' => $validated['password'],
            'email_verified_at' => new Carbon()
        ]);

        $request->session()->flash('status', 'created-success');

        return redirect()->intended(route('admin.users.index'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Request $request, $id) {
        $data = User::findOrFail($id);
        return view('admin.users.edit', [
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(UpdateRequest $request, $id) {
        $validated = $request->validated();
        $user = User::findOrFail($id);
        $user->update([
            'name' => $validated['name'],
            'status' => $validated['status'],
            'is_admin' => $validated['is_admin'],
            'email' => $validated['email'],
        ]);

        $password = trim($request->get('password'));


        if ($password) {
            $validated = $request->validate([
                'password' => 'required|string|confirmed|min:8',
            ]);
            $user->update([
                'password' => $validated['password']
            ]);
        }

        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.users.edit', array('id' => $validated['id'])));
    }

    public function delete(Request $request, $id) {
        $data = User::findOrFail($id);

        return view('admin.users.delete', [
            'data' => $data
        ]);
    }

    public function remove(DeleteRequest $request, $id) {
        $user = User::findOrFail($id);

        UserAnswer::where([
            ['user_id', '=', $user->id]
        ])->delete();

        $user->delete();

        return redirect()->intended(route('admin.users.index'));
    }
}
