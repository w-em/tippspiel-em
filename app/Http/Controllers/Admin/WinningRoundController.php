<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\User\DeleteRequest;
use App\Http\Requests\Admin\WinningRound\CreateRequest;
use App\Http\Requests\Admin\WinningRound\EditRequest;
use App\Http\Requests\Admin\WinningRound\StoreRequest;
use App\Http\Requests\Admin\WinningRound\UpdateRequest;
use App\Models\Question;
use App\Models\Team;
use App\Models\WinRound;
use App\Models\WinRoundQuestion;
use Illuminate\Http\Request;

class WinningRoundController extends AdminController
{
    /**
     * winrounds
     *
     * here all winrounds
     */
    public function index(Request $request) {
        return view('admin.winning_rounds.index', [
            'rounds' => WinRound::where([['league_id', '=', 1]])->get()
        ]);
    }

    /**
     * show the form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(CreateRequest $request) {
        return view('admin.winning_rounds.create', [
            'old' => $request->old(),
            'questions' => Question::where([['league_id', '=', 1]])->get()
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(StoreRequest $request) {
        $validated = $request->validated();

        $winRound = WinRound::create([
            'name' => $validated['name'],
            'league_id' => $this->currentLeague->id,
        ]);

        foreach ($validated['selected'] as $questionId) {
            WinRoundQuestion::create([
                'win_round_id' => $winRound->id,
                'value' => (int) $questionId,
            ]);
        }

        $request->session()->flash('status', 'created-success');

        return redirect()->intended(route('admin.winning_rounds.index'));
    }

    /**
     * @param EditRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(EditRequest $request, $id) {

        $winRound = WinRound::with(['questions'])->findOrFail($id);

        return view('admin.winning_rounds.edit', [
            'data' => $winRound,
            'old' => $request->old(),
            'questions' => Question::where([['league_id', '=', 1]])->get()
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(UpdateRequest $request, $id) {
        $validated = $request->validated();

        $winRound = WinRound::findOrFail($id);

        WinRoundQuestion::where([
            'win_round_id' => $winRound->id
        ])->delete();

        foreach ($validated['selected'] as $questionId) {
            WinRoundQuestion::create([
                'win_round_id' => $winRound->id,
                'value' => (int) $questionId,
            ]);
        }

        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.winning_rounds.edit', array('id' => $validated['id'])));
    }

    public function delete(Request $request, $id) {
        $data = WinRound::findOrFail($id);

        return view('admin.winning_rounds.delete', [
            'data' => $data
        ]);
    }

    public function remove(DeleteRequest $request, $id) {
        $round = WinRound::findOrFail($id);
        WinRoundQuestion::where([
            ['win_round_id', '=', $id]
        ])->delete();

        $round->delete();

        return redirect()->intended(route('admin.winning_rounds.index'));
    }
}
