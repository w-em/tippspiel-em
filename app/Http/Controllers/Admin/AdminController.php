<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\BaseController;
use App\Http\Requests\Admin\Question\StoreRequest;
use App\Models\GameSchedule;
use App\Models\PointRule;
use App\Models\Question;
use App\Models\Team;
use App\Models\User;
use App\Models\WinRound;
use App\Models\WinRoundQuestion;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AdminController extends BaseController
{
    protected $defaultPaginationPerPage = 50;

    public function index(Request $request) {
        return view('admin.index');
    }

    public function gamesIndex() {
        return view('admin.games.index');
    }
}
