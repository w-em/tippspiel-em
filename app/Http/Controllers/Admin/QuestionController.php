<?php


namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\Question\CreateRequest;
use App\Http\Requests\Admin\Question\EditRequest;
use App\Http\Requests\Admin\Question\IndexRequest;
use App\Http\Requests\Admin\Question\StoreRequest;
use App\Http\Requests\Admin\Question\UpdateRequest;
use App\Http\Requests\Admin\User\DeleteRequest;
use App\Models\GameSchedule;
use App\Models\PointRule;
use App\Models\Question;
use App\Models\Team;
use App\Models\UserAnswer;
use Illuminate\Http\Request;

class QuestionController extends AdminController
{
    /**
     * Questions
     *
     * here all Questions
     */
    public function index(Request $request) {
        $questions = Question::orderBy('type')->orderBy('start_date')->paginate($this->defaultPaginationPerPage);
        $questions::defaultView('pagination::bootstrap-4');
        return view('admin.questions.index', [
            'questions' => $questions
        ]);
    }

    /**
     * @param $validated
     * @return string
     */
    protected function getQuestionByValidation($validated) {
        $question = '';
        if ((int) $validated['type'] === Question::TYPE_GAME) {
            $question = $validated['home'] . ':' . $validated['guest'];
        }
        elseif((int) $validated['type'] === Question::TYPE_BONUS) {
            $trimedarray = implode(',', array_map("trim",$validated['selection']));
            $question = $validated['question'] .  '#' . $trimedarray;
        }

        return $question;
    }

    /**
     * show the form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(CreateRequest $request) {
        $pointRules = PointRule::get();
        $schedules = GameSchedule::get();
        return view('admin.questions.create', [
            'old' => $request->old(),
            'type' => Question::TYPE_GAME,
            'point_rules' => $pointRules,
            'schedules' => $schedules,
            'teams' => Team::orderBy('name')->get()
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function store(StoreRequest $request) {

        $validated = $request->validated();

        $question = $this->getQuestionByValidation($validated);

        Question::create([
            'type' => $validated['type'],
            'point_rule' => $validated['point_rule'],
            'question' => $question,
            'start_date' => $validated['start_date'],
            'league_id' => $this->currentLeague->id,
            'game_schedule_id' => $validated['game_schedule_id'],
            'max_tipp_date' => $validated['max_tipp_date'],
        ]);

        $request->session()->flash('status', 'created-success');

        return redirect()->intended(route('admin.questions.index'));
    }

    /**
     * @param EditRequest $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(EditRequest $request, $id) {
        $question = Question::findOrFail($id);
        $pointRules = PointRule::get();
        $schedules = GameSchedule::get();
        return view('admin.questions.edit', [
            'type' => $question->type,
            'old' => $request->old(),
            'question' => $question,
            'schedules' => $schedules,
            'point_rules' => $pointRules,
            'teams' => Team::orderBy('name')->get()
        ]);
    }

    /**
     * store the input to db
     * @param StoreRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function update(UpdateRequest $request) {

        $validated = $request->validated();
        $questionValue = $this->getQuestionByValidation($validated);

        $question = Question::findOrFail((int) $validated['id']);
        $question->update([
            'type' => $validated['type'],
            'point_rule' => $validated['point_rule'],
            'question' => $questionValue,
            'start_date' => $validated['start_date'],
            'league_id' => $this->currentLeague->id,
            'game_schedule_id' => $validated['game_schedule_id'],
            'max_tipp_date' => $validated['max_tipp_date'],
        ]);

        $request->session()->flash('status', 'updated-success');

        return redirect()->intended(route('admin.questions.edit', array('id' => $validated['id'])));

    }

    public function delete(Request $request, $id) {
        $question = Question::findOrFail($id);
        return view('admin.questions.delete', [
            'data' => $question
        ]);
    }

    public function remove(DeleteRequest $request, $id) {
        $question = Question::findOrFail($id);

        UserAnswer::where([
            ['question_id', '=', $id]
        ])->delete();
        $question->delete();
        return redirect()->intended(route('admin.questions.index'));

    }
}
