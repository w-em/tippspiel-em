<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tipp\AnswerSaveRequest;
use App\Models\UserAnswer;
use App\Models\Question;
use App\Models\GameSchedule;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class TippController extends BaseController
{
    protected $gameSchedules = null;

    protected function overview (Request $request, $id = null) {

        $this->user = Auth::user();

        $gameSchedules = $this->getGameSchedules();
        $selectedGameSchedule = $this->getSelectedGameSchedule($id);

        $games = Question::where([
            ['game_schedule_id', '=', $selectedGameSchedule->id]
        ])->orderBy('start_date', 'asc')->get();

        return view('tipp.overview', [
            'game_schedules' => $gameSchedules,
            'selected_schedule' => $selectedGameSchedule,
            'games' => $games,
        ]);
    }

    /**
     * function to show the tip list
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function gameTipp (Request $request, $id = null) {

        $this->user = Auth::user();

        $gameSchedules = $this->getGameSchedules();
        $selectedGameSchedule = $this->getSelectedGameSchedule($id);

        $games = Question::where([
            ['game_schedule_id', '=', $selectedGameSchedule->id],
        ])->orderBy('start_date', 'asc')->get();

        return view('tipp.game', [
            'old' => $request->old(),
            'game_schedules' => $gameSchedules,
            'selected_schedule' => $selectedGameSchedule,
            'games' => $games,
        ]);
    }

    /**
     * save the current user tipp request
     * @param Request $request
     */
    protected function gameTippSave(AnswerSaveRequest $request) {

        $this->user = Auth::user();

        $validated = $request->validated();
        $tipps = $validated['tipp'];

        foreach ($tipps as $questionId => $tip) {
            $this->saveTip($questionId, $tip);
        }

        return Redirect::back();
    }

    /**
     * save the tip to database
     *
     * @param $questionId
     * @param $tip
     */
    protected function saveTip($questionId, $tip) {

        $question = Question::findOrFail((int) $questionId);

        if ($question->canTipp === false) {
            return false;
        }

        if ($question->type === Question::TYPE_GAME) {
            if (!is_array($tip)) {
                return false;
            }

            if (is_null($tip['home']) || is_null($tip['guest'])) {
                return false;
            }

            $homeTip = (int) $tip['home'];
            $guestTip = (int) $tip['guest'];

            $data = $homeTip . ':' . $guestTip;
        } elseif ($question->type === Question::TYPE_BONUS) {
            $data = (int) $tip;
        }

        UserAnswer::updateOrCreate(
            [
                'user_id' => $this->user->id,
                'question_id' => $questionId
            ],
            [
                'data' => $data
            ]
        );
    }

    /**
     * get the current SelectedGameSchedule
     * @param null $id
     * @return mixed
     */
    protected function getSelectedGameSchedule($id = null) {
        if ($id === null) {
            $selectedGameSchedule = $this->getGameSchedules()->first();
        } else {
            $selectedGameSchedule = GameSchedule::where([
                ['id', '=', $id],
                ['league_id', '=', $this->currentLeague->id]
            ])->first();

            if ($selectedGameSchedule === null) {
                $selectedGameSchedule = $this->getGameSchedules()->first();
            }
        }

        return $selectedGameSchedule;
    }

    /**
     * @return Collection
     */
    protected function getGameSchedules () {
        if ($this->gameSchedules === null) {
            $this->gameSchedules = GameSchedule::where([
                ['league_id', '=', $this->currentLeague->id]
            ])->orderBy('end_date', 'asc')->get();
        }

        return $this->gameSchedules;
    }

}
