<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StaticController extends BaseController
{
    protected $gameSchedules = null;

    protected function privacyPolicy (Request $request) {
        return view('static.privacy-policy');
    }

    protected function cookies (Request $request) {
        return view('static.cookies');
    }

    protected function impress (Request $request) {
        return view('static.impress');
    }

    protected function terms (Request $request) {
        return view('static.terms');
    }

    protected function wins (Request $request) {
        return view('static.wins');
    }

    /**
     * regeln
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function rules (Request $request) {
        return view('static.rules');
    }

}
