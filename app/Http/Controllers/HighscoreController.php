<?php

namespace App\Http\Controllers;

use App\Models\GameSchedule;
use App\Models\User;
use App\Models\WinRound;
use App\Services\HighscoreService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HighscoreController extends BaseController
{
    protected const INCLUDE_ADMINS = false;

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    protected function index (Request $request, $id = null) {
        $winRounds = WinRound::with(['questions'])->where([
            ['league_id', '=', $this->currentLeague->id]
        ])->get();

        if ($id === null) {
            $selected = $winRounds->first();
        }
        else {
            $selected = WinRound::with(['questions'])
                ->where([
                    ['league_id', '=', $this->currentLeague->id],
                    ['id', '=', $id]
                ])->get()
                ->first();

            if ($selected === null) {
                $selected = $winRounds->first();
            }
        }

        $questionIds = [];

        if ($selected) {
            $questionIds = $selected->questions->pluck('value')->toArray();
        }

        $users = $this->getHighscoreBaseQuery($questionIds)->paginate($this->defaultPaginationPerPage);
        $users::defaultView('pagination::bootstrap-4');

        return view('highscore.index', [
            'winRounds' => $winRounds,
            'selected' => $selected,
            'users' => $users,
            // 'tests' => $this->getHighscoreBaseQuery($questionIds)->get(),
            'currentUserHighscore' => Auth::user() ? $this->getCurrentUserHighscorePositionByQuestionIds($questionIds, Auth::user()) : null
        ]);
    }

    /**
     * @param $questionIds
     * @param $currentUser
     * @return mixed|null
     */
    protected function getCurrentUserHighscorePositionByQuestionIds($questionIds, $currentUser) {

        if (!$currentUser) {
            return null;
        }

        $highscoreRows = $this->getHighscoreBaseQuery($questionIds)->get();
        foreach ($highscoreRows as $highscoreRow) {
            if ($highscoreRow->id === $currentUser->id) {
                return $highscoreRow;
            }
        }

        return null;

    }

    /**
     * @param $questionIds
     * @return query
     */
    protected function getHighscoreBaseQuery($questionIds) {
        $positions = $this->getGroupedPointsStringByQuestionIds($questionIds);

        $whereClause = [];
        $whereClause[] = [
            'status', '=', User::STATUS_ACTIVE
        ];

        if (self::INCLUDE_ADMINS === false) {
            $whereClause[] = [
                'is_admin', '=', 0
            ];
        }

        return User::select(
            'users.id',
            'users.is_admin',
            'users.name',
            DB::raw('SUM(user_answers.points) as points'),
            DB::raw('FIND_IN_SET(SUM(user_answers.points),"'.$positions.'") AS position')
        )->leftJoin('user_answers', 'user_answers.user_id', '=', 'users.id')
            ->whereIn('question_id', $questionIds)
            ->groupBy([
                'users.id'
            ])
            ->orderBy('points', 'DESC')
            ->where($whereClause);
    }

    /**
     * @param array $questionIds
     * @return string
     */
    protected function getGroupedPointsStringByQuestionIds(array $questionIds) {
        if (count($questionIds) === 0) {
            return '';
        }

        $sql = "
            SELECT
                GROUP_CONCAT(DISTINCT points ORDER BY points DESC) AS points
            FROM (
                SELECT
                    sum(points) AS points
                FROM
                    users
                LEFT JOIN `user_answers` ON `user_answers`.`user_id` = `users`.`id`
            WHERE
                `question_id` in(" . implode(',', $questionIds) . ")
                and(`status` = ?)
            GROUP BY
                users.id) AS t
        ";

        $data = DB::select($sql, [User::STATUS_ACTIVE]);
        return $data[0]->points;
    }
}
