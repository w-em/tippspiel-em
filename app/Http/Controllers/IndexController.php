<?php

namespace App\Http\Controllers;

class IndexController extends BaseController
{
    protected function indexAction () {
        return view('static.welcome');
    }
}
