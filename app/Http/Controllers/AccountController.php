<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\ChangePasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends BaseController
{
    protected function index (Request $request) {

        return view('user.account.index', [
            'user' => $request->user()
        ]);
    }

    protected function changePasswordIndex (Request $request) {

        return view('user.account.password_change.index', [
            'user' => $request->user()
        ]);
    }

    /**
     * @param ChangePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function changePasswordSave(ChangePasswordRequest $request) {

        $data = $request->only('old_password', 'password', 'password_confirmation');

        $currentPassword = Auth::user()->password;
        if(Hash::check($data['old_password'], $currentPassword)) {
            $user = User::find(Auth::user()->id);
            $user->password = $data['password'];
            $user->save();
        }

        $request->session()->flash('status', 'password-updated-success');

        return redirect()->intended(route('user.account.index'));
    }
}
