<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class EmailVerificationPromptController extends BaseController
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if ($user && $user->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::HOME);
        } else {
            return view('auth.verify-email');
        }
    }
}
