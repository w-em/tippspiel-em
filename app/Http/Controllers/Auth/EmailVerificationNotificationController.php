<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models\User;
use App\Rules\ReCaptcha;
use Illuminate\Http\Request;

class EmailVerificationNotificationController extends BaseController
{
    /**
     * Send a new email verification notification.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|email',
            'recaptcha' => [
                'required',
                new ReCaptcha
            ]
        ]);

        $user = User::where([
            ['email', '=', $validated['email']],
        ])->first();

        if (!$user) {

        } else {
            if (!$user->hasVerifiedEmail()) {
                $user->sendEmailVerificationNotification();
            }
        }

        return back()->with('status', 'verification-link-sent');

    }
}
