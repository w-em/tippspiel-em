<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use App\Http\Requests\Auth\EmailVerificationRequest;

class VerifyEmailController extends BaseController
{
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \App\Http\Requests\Auth\EmailVerificationRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(EmailVerificationRequest $request, $id, $hash)
    {
        try {
            $user = User::findOrFail($id);

            $isCorrectUser = hash_equals((string) $id, (string) $user->getKey());
            $isCorrectEmail = hash_equals((string) $hash, sha1((string) $user->getEmailForVerification()));

            if ($user && $isCorrectUser && $isCorrectEmail) {
                if (!$user->hasVerifiedEmail()) {
                    $user->markEmailAsVerified();
                    event(new Verified($user));
                }

                if ($user->hasVerifiedEmail()) {
                    $request->session()->flash('status', 'verification-success');
                    return redirect()->intended(route('login'));
                }

                return redirect()->intended(route('login'));
            }
        } catch (\Exception $ex) {
            $request->session()->flash('status', 'verification-failed');
            return redirect()->intended('/login'.'?verified=0');
        }

    }
}
