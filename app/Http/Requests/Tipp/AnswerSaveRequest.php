<?php

namespace App\Http\Requests\Tipp;

use App\Models\Question;
use App\Rules\ReCaptcha;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AnswerSaveRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * TODO: Check is time possible
     */
    public function rules()
    {
        $rules = [
            'tipp' => 'required|array',
            'schedule' => 'required|integer|exists:game_schedule,id',
            'recaptcha' => [
                'required',
                new ReCaptcha
            ]
        ];

        $tipps = $this->tipp;


        foreach ($tipps as $questionId => $bet) {
            $question = Question::find($questionId);
            if ($question) {
                $isPast = $question->max_tipp_date->isPast();

                if($isPast) {
                    $rules['tipp.'.$questionId . '.date'] = 'required|date|before:' . $question->max_tipp_date->format('Y-m-d H:i:s');
                }

                if ($question->type === Question::TYPE_GAME) {

                    if (is_null($bet['home']) && is_null($bet['guest'])) {
                        continue;
                    }

                    $rules['tipp.'.$questionId . '.home'] = 'required|integer|gte:0';
                    $rules['tipp.'.$questionId . '.guest'] = 'required|integer|gte:0';
                }
                if ($question->type === Question::TYPE_BONUS) {
                    $rules['tipp.'.$questionId] = [
                        'required',
                        'integer',
                        Rule::in($question->bonusSelectables->pluck('id')->toArray()),
                    ];

                    if($isPast) {
                        $rules['tipp.'.$questionId] = 'date|before:'  . $question->max_tipp_date->format('Y-m-d H:i:s');
                    }
                }
            }
        }

        return $rules;
    }
}
