<?php

namespace App\Http\Requests\User;

use App\Rules\ReCaptcha;
use App\Rules\StrongPassword;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => [
                'required',
                'min:8',
                new StrongPassword,
            ],
            'password_confirmation' => 'required|same:password',
            'terms' => 'required',
            'privacy' => 'required',
            'recaptcha' => [
                'required',
                new ReCaptcha
            ]
        ];
    }
}
