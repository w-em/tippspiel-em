<?php


namespace App\Http\Requests\User;


use App\Rules\ReCaptcha;
use App\Rules\StrongPassword;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ChangePasswordRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required',
            'password' => [
                'required',
                'min:8',
                new StrongPassword,
            ],
            'password_confirmation' => 'required|same:password',
            'recaptcha' => [
                'required',
                new ReCaptcha
            ]
        ];
    }
}
