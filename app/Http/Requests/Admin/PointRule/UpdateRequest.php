<?php

namespace App\Http\Requests\Admin\PointRule;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ((int) $this->type === 0) {
            return [
                'id' => 'required|exists:point_rules',
                'name' => 'required|string',
                'type' => 'required|integer',
                'exact' => 'required|integer',
                'tendence' => 'required|integer',
                'difference' => 'required|integer',
            ];
        } elseif ((int) $this->type === 1) {
            return [
                'id' => 'required|exists:point_rules',
                'name' => 'required|string',
                'type' => 'required|integer',
                'exact' => 'required|integer',
            ];
        } else {
            throw new \InvalidArgumentException('invalid type');
        }
    }
}
