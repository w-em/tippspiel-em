<?php

namespace App\Http\Requests\Admin\PointRule;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ((int) $this->type === 0) {
            return [
                'name' => 'required|string',
                'type' => 'required|integer',
                'exact' => 'required|integer',
                'tendence' => 'required|integer',
                'difference' => 'required|integer',
            ];
        } elseif ((int) $this->type === 1) {
            return [
                'name' => 'required|string',
                'type' => 'required|integer',
                'exact' => 'required|integer',
            ];
        } else {
            throw new \InvalidArgumentException('invalid type');
        }

    }
}
