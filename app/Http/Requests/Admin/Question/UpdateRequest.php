<?php

namespace App\Http\Requests\Admin\Question;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'type' => 'required|integer',
            'point_rule' => 'required|string',
            'start_date' => 'required_if:type,0|date|nullable',
            'max_tipp_date' => 'required|date',
            'game_schedule_id' => 'required|integer',
            'question' => 'required_if:type,1|string|nullable',
            'selection' => 'required_if:type,1|array|nullable',
            'home' => 'required_if:type,0|integer',
            'guest' => 'required_if:type,0|integer',
        ];
    }
}
