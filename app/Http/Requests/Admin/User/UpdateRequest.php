<?php


namespace App\Http\Requests\Admin\User;


use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'status' => 'required|integer',
            'is_admin' => 'required|integer',
            'name' => 'required|string|unique:users,name,'.$this->id . ',id',
            'email' => 'required|string|email|max:255|unique:users,email,'.$this->id . ',id',
        ];
    }
}
