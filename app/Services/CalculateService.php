<?php

namespace App\Services;

use App\Models\Question;
use App\Models\User;
use App\Models\UserAnswer;

class CalculateService
{

    /**
     * calculate the points for all users to a question
     * @param Question $question
     */
    public static function calculateQuestion(Question $question)
    {

        if (is_null($question->answer)) {
            return;
        }



        $groupedAnswers = UserAnswer::where([
                'question_id' => $question->id
            ])
            ->groupBy(['data'])
            ->get();



        foreach($groupedAnswers as $answer) {

            $pointsEarned = self::earnPoints($question, $answer->answer);

            UserAnswer::where([
                'question_id' => $question->id,
                'data' => $answer->getOriginal('data')
            ])->update([
                'points' => $pointsEarned
            ]);
        }
    }

    /**
     * calculate the complete user
     * @param User $user
     */
    public static function calculateUser(User $user)
    {
        $answers = UserAnswer::with(['question'])->where([
            'user_id' => $user->id
        ])->get();

        foreach ($answers as $answer) {
            self::calculateAnswer($answer);
        }
    }

    /**
     * @param UserAnswer $answer
     * @return UserAnswer
     */
    protected static function calculateAnswer(UserAnswer $answer)
    {
        $pointsEarned = self::earnPoints($answer->question, $answer->answer);

        $answer->points = $pointsEarned;
        $answer->save();

        return $answer;
    }

    /**
     * @param Question $question
     * @param $result
     * @param $userAnswer
     * @return int
     */
    protected static function earnPoints(Question $question, $userAnswer)
    {
        if ($question->type === Question::TYPE_GAME) {
            return self::earnPointsByGame($question, $userAnswer);
        } elseif ($question->type === Question::TYPE_BONUS) {
            return self::earnPointsByBonus($question, $userAnswer);
        }

        return 0;
    }

    /**
     * how much points earned by game
     * @param $answer
     * @param $userAnswer
     * @return int
     */
    protected static function earnPointsByGame(Question $question, $userAnswer)
    {

        $betResultAnalyzer = new BetResultAnalyzer();
        $betResultAnalyzer->setRealResult((int)$question->answer['home'], (int)$question->answer['guest']);
        $betResultAnalyzer->setBetResult((int)$userAnswer['home'], (int)$userAnswer['guest']);

        $data = $question->pointRule->data;

        if ($betResultAnalyzer->isExactResult()) {
            return (is_array($data) && key_exists('exact', $data)) ? (int)$data['exact'] : 0;
        } elseif ($betResultAnalyzer->isSameTendency()) {
            return (is_array($data) && key_exists('tendence', $data)) ? (int)$data['tendence'] : 0;
        } elseif ($betResultAnalyzer->isSameScoreDifference()) {
            return (is_array($data) && key_exists('difference', $data)) ? (int)$data['difference'] : 0;
        }  else {
            return 0;
        }
    }

    /**
     * @param Question $question
     * @param $userAnswer
     */
    protected static function earnPointsByBonus(Question $question, $userAnswer)
    {
        try {
            if($question->answer->id === $userAnswer->id) {
                $data = $question->pointRule->data;
                return (is_array($data) && key_exists('exact', $data)) ? (int)$data['exact'] : 0;
            }
        } catch(\Exception $exception) {
            return 0;
        }
    }
}
