<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Services\ActionService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class CheckuserExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:userexpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user expired status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::where([
            ['created_at', '<', (now())->subHours(25)]
        ])->whereNull(['email_verified_at'])
            ->delete();

    }
}
