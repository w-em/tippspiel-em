<?php

namespace App\Console\Commands;

use App\Models\Question;
use App\Services\CalculateService;

use Illuminate\Console\Command;

class CalculateQuestions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:questions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate all questions with given user answers';

    /**
     * @var CalculateService
     */
    protected $calculateService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CalculateService $calculateService)
    {

        $this->calculateService = $calculateService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $questions = Question::whereNotNull('answer')->get();
        foreach($questions as $question) {
            CalculateService::calculateQuestion($question);
        }

    }
}
