<?php

namespace App\Csp;

use Spatie\Csp\Policies\Policy;
use Spatie\Csp\Directive;
use Spatie\Csp\Keyword;

class Basic extends Policy
{
    public function configure()
    {
        $this
            ->addDirective(Directive::BASE, Keyword::SELF)
            ->addDirective(Directive::CONNECT, Keyword::SELF)
            ->addDirective(Directive::DEFAULT, Keyword::SELF)

            ->addDirective(Directive::FORM_ACTION, Keyword::SELF)
            ->addDirective(Directive::IMG, Keyword::SELF)
            ->addDirective(Directive::MEDIA, Keyword::SELF)
            ->addDirective(Directive::OBJECT, Keyword::NONE)
            ->addDirective(Directive::SCRIPT, Keyword::SELF)
            ->addDirective(Directive::STYLE, Keyword::SELF)
            ->addDirective(Directive::STYLE, Keyword::UNSAFE_INLINE)
            ->addDirective(Directive::SCRIPT, Keyword::UNSAFE_INLINE)
            //->addNonceForDirective(Directive::SCRIPT)
            // ->addNonceForDirective(Directive::STYLE)
            ->addDirective(Directive::FONT, Keyword::SELF)
            ->addDirective(Directive::FONT, 'data:')
            ->addDirective(Directive::IMG, 'data:')
            ->addDirective(Directive::FRAME, 'https://consentcdn.cookiebot.com/')
            ->addDirective(Directive::FRAME, 'https://www.google.com/recaptcha/')
            ->addDirective(Directive::SCRIPT, 'https://www.google.com')
            ->addDirective(Directive::SCRIPT, 'https://www.googletagmanager.com')
            ->addDirective(Directive::SCRIPT, 'https://ssl.gstatic.com')
            ->addDirective(Directive::SCRIPT, 'https://www.gstatic.com')
            ->addDirective(Directive::SCRIPT, 'https://www.facebook.com')
            ->addDirective(Directive::SCRIPT, 'https://consent.cookiebot.com')
            ->addDirective(Directive::SCRIPT, 'https://consentcdn.cookiebot.com/')
            ->addDirective(Directive::SCRIPT, 'https://connect.facebook.net/')
            ->addDirective(Directive::SCRIPT, 'https://www.googleadservices.com/')
            ->addDirective(Directive::SCRIPT, 'https://googleads.g.doubleclick.net/')
            ->addDirective(Directive::IMG, 'https://www.facebook.com/')
            ->addDirective(Directive::IMG, 'https://via.placeholder.com/')
            ->addDirective(Directive::IMG, 'https://www.google.de/')
            ->addDirective(Directive::IMG, 'https://www.google.com/')

            // bing
            ->addDirective(Directive::SCRIPT, 'https://www.bing.com')
            ->addDirective(Directive::SCRIPT, 'https://www.google-analytics.com')
            ->addDirective(Directive::CONNECT, 'https://www.google-analytics.com')
            ->addDirective(Directive::CONNECT, 'https://stats.g.doubleclick.net')
            ->addDirective(Directive::IMG, 'https://www.google-analytics.com')
            ->addDirective(Directive::FRAME, 'https://www.google-analytics.com')
            ->addDirective(Directive::CONNECT, 'https://www.google.com')
            ->addDirective(Directive::CONNECT, 'https://www.google.de')
            ->addDirective(Directive::CONNECT, 'https://www.facebook.com')
            ->addDirective(Directive::CONNECT, 'https://googleads.g.doubleclick.net')
            ->addDirective(Directive::SCRIPT, Keyword::UNSAFE_EVAL)
        ;
    }
}
