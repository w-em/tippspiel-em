<?php

namespace App\Jobs;

use App\Models\Question;
use App\Services\CalculateService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessQuestionCalculateJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $model = null;

    /**
     * Delete the job if its models no longer exist.
     *
     * @var bool
     */
    public $deleteWhenMissingModels = true;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    function __construct(Question $model)
    {
        $this->model = $model;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        CalculateService::calculateQuestion($this->model);
    }
}
