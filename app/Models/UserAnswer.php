<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserAnswer extends Model
{

    protected $table = 'user_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'question_id',
        'data',
        'points',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    protected $appends = ['answer'];

    /**
     * get the question
     * @return HasOne Question
     */
    public function question()
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }

    public function getAnswerAttribute () {
        if ($this->question->type === Question::TYPE_GAME) {
            $tipp = explode(':', $this->data);
            if (count($tipp) === 2) {
                return [
                    'home' => (int) $tipp[0],
                    'guest' => (int) $tipp[1],
                ];
            }
        }
        elseif ($this->question->type === Question::TYPE_BONUS) {
            try {
                return Team::findOrFail((int) $this->data);
            } catch(\Exception $ex) {
                return null;
            }
        }

        return null;
    }
}
