<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GameSchedule extends Model
{
    protected $table = 'game_schedule';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date',
        'end_date',
        'name',
        'short_name',
        'league_id',
        'show_groups'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
        'show_groups' => 'boolean',
    ];
}
