<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WinRound extends Model
{

    protected $table = 'win_rounds';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'name',
        'data',
        'league_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'deleted_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $appends = [];

    public function questions() {
        return $this->hasMany(WinRoundQuestion::class, 'win_round_id', 'id');
    }
}
