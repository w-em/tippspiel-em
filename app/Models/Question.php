<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Question extends Model
{

    const TYPE_GAME = 0;
    const TYPE_BONUS = 1;

    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'answer',
        'question',
        'start_date',
        'league_id',
        'game_schedule_id',
        'max_tipp_date',
        'point_rule',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime',
        'max_tipp_date' => 'datetime',
    ];

    protected $appends = ['homeTeam', 'guestTeam', 'userAnswer', 'userPoints', 'canTipp', 'bonusSelectables', 'bonusQuestion', 'label', 'adminLabel'];


    public function getAdminLabelAttribute() {
        if ($this->type === self::TYPE_GAME) {
            try {
                return  $this->homeTeam->name . ' ('.$this->homeTeam->group.') - ' . $this->guestTeam->name . ' ('.$this->guestTeam->group.')';
            } catch(\Exception $ex) {
                return 'unbekannt - unbekannt';
            }
        } elseif ($this->type === self::TYPE_BONUS) {
            return $this->getBonusQuestionAttribute();
        } else {
            return 'Type is undefined';
        }
    }

    /**
     * @return mixed|string
     */
    public function getLabelAttribute() {
        if ($this->type === self::TYPE_GAME) {
            try {
                return  $this->homeTeam->name . ' - ' . $this->guestTeam->name;
            } catch(\Exception $ex) {
                return 'unbekannt - unbekannt';
            }
        } elseif ($this->type === self::TYPE_BONUS) {
            return $this->getBonusQuestionAttribute();
        } else {
            return 'Type is undefined';
        }
    }

    /**
     * @param $val
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pointRule() {
        return $this->hasOne(PointRule::class, 'id', 'point_rule');
    }

    /**
     * get the home team by question field attribute
     * @return Team|null
     */
    public function getHomeTeamAttribute() {
        if ($this->type !== self::TYPE_GAME) {
            return null;
        }

        $teamIds = explode(':', $this->question);
        $teamId = (int) $teamIds[0];
        if ($teamId <= 0) {
            return null;
        }

        return Team::findOrFail($teamId);
    }

    /**
     * get the home team by question field attribute
     * @return Team|null
     */
    public function getGuestTeamAttribute() {
        if ($this->type !== self::TYPE_GAME) {
            return null;
        }

        $teamIds = explode(':', $this->question);
        $teamId = (int) $teamIds[1];
        if ($teamId <= 0) {
            return null;
        }

        return Team::findOrFail($teamId);
    }

    /**
     * @return int[]|void|null
     */
    public function getUserAnswerAttribute() {
        $answers = UserAnswer::where([
            ['user_id', '=', Auth::user()->id],
            ['question_id', '=', $this->id]
        ])->get();

        if ($answers->count() !== 1) {
            return false;
        }

        $answer =  $answers->first();
        return $answer->answer;
    }

    /**
     * @return mixed|string
     */
    public function getBonusQuestionAttribute() {
        if ($this->type === Question::TYPE_BONUS) {
            $ex = explode('#', $this->question);
            return $ex[0];
        } else {
            return $this->question;
        }
    }

    public function getBonusSelectablesAttribute() {
        if ($this->type === Question::TYPE_BONUS) {
            $ex = explode('#', $this->question);
            try {
                return Team::whereIn('id', explode(',', $ex[1]))->orderBy('name')->get();
            } catch(\Exception $ex) {
                return [];
            }
        }

        return [];
    }

    /**
     * @param $val
     * @return int[]|null
     */
    public function getAnswerAttribute ($val) {
        if ($this->type === Question::TYPE_GAME) {
            $tipp = explode(':', $val);
            if (count($tipp) === 2) {
                return [
                    'home' => (int) $tipp[0],
                    'guest' => (int) $tipp[1],
                ];
            }
        }
        elseif ($this->type === Question::TYPE_BONUS) {
            if ((int) $val > 0) {
                $team = Team::find((int) $val);
                return $team ? $team : $val;
            }
        }
        else {
            return $val;
        }

        return null;
    }

    public function getUserPointsAttribute() {
        $answers = UserAnswer::where([
            ['user_id', '=', Auth::user()->id],
            ['question_id', '=', $this->id]
        ])->get();

        if ($answers->count() === 1) {
            $answer = $answers->first();
            return (int) $answer->points;
        }
        return 0;
    }

    public function getCanTippAttribute () {
        $maxTippDate = $this->max_tipp_date;
        $nowDate = Carbon::now();

        return !($nowDate->gt($maxTippDate) || $nowDate->eq($maxTippDate));
    }
}
