<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;

class VerifyEmail extends Notification
{
    /**
     * The callback that should be used to build the mail message.
     *
     * @var \Closure|null
     */
    public static $toMailCallback;

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $verificationUrl = $this->verificationUrl($notifiable);

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $verificationUrl);
        }

        return (new MailMessage)
            ->template('vendor.notifications.verify_email')
            ->from('no-reply@lidl-tps.at', 'Lidl EM-Tippspiel ')
            ->subject('Registrierung beim Lidl EM-Tippspiel')
            ->greeting('Lieber Fußballfan,')
            ->line('du hast es fast geschafft. Bitte bestätige innerhalb von 24 Stunden deine Anmeldung zum Lidl EM-Tippspiel 2021, ansonsten verfällt deine Registrierung.')
            ->action(Lang::get('REGISTRIERUNG BESTÄTIGEN'), $verificationUrl)
            ->line('Nach erfolgreicher Bestätigung kannst du mit dem Tippen sofort loslegen!')
            ->line('Bitte beachte, dass Rückmeldungen an diese E-Mail-Adresse nicht beantwortet werden.');

    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    public function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addHours(24),
            [
                'id' => $notifiable->getKey(),
                'hash' => sha1($notifiable->getEmailForVerification()),
            ]
        );
    }

    /**
     * Set a callback that should be used when building the notification mail message.
     *
     * @param  \Closure  $callback
     * @return void
     */
    public static function toMailUsing($callback)
    {
        static::$toMailCallback = $callback;
    }
}
