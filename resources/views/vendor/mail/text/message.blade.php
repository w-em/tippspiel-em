@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => 'https://www.lidl.at'])
            LIDL Selection
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
UNSERE IMPRESSUMSANGABEN
Lidl Österreich GmbH
Unter der Leiten 11
5020 Salzburg

Tel.: 0800/500 810
Mail: kundenservice@lidl.at

Sitz in Neckarsulm, Registergericht Stuttgart, HRB 105671
FN: 131556s
Landesgericht Salzburg
UID: ATU37893801

Herausgeber und Medienhaber:
Lidl Österreich GmbH
Unter der Leiten 11
5020 Salzburg
        @endcomponent
    @endslot
@endcomponent
