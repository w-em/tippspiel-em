<tr class="footer">
<td class="footer-wrapper">
<table class="footer" align="center" width="570" bgcolor="#6D757C" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="content-cell" align="center">
    <h2>UNSERE IMPRESSUMSANGABEN</h2>
    <p>
        Lidl Österreich GmbH<br>
        Unter der Leiten 11<br>
        5020 Salzburg<br>
        <br>
        Tel.: 0800/500 810<br>
        Mail: <a style="color: #ffffff;"href="mailto:kundenservice@lidl.at">kundenservice@lidl.at</a><br>
        <br>
        Sitz in Neckarsulm, Registergericht Stuttgart, HRB 105671<br>
        FN: 131556s<br>
        Landesgericht Salzburg<br>
        UID: ATU37893801<br>
        <br>
        Herausgeber und Medienhaber:<br>
        Lidl Österreich GmbH<br>
        Unter der Leiten 11<br>
        5020 Salzburg<br>
    </p>
</td>
</tr>
</table>
</td>
</tr>
