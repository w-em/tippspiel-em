@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => 'https://lidl.at'])
            <img src="{{ asset('images/email/brand__at.png') }}" style="max-width: 120px; width:120px; height: 105px"  width="120" height="105" class="logo" alt="LIDL AT">
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            <h2>UNSERE IMPRESSUMSANGABEN</h2>
            <p>
                Lidl Österreich GmbH<br>
                Unter der Leiten 11<br>
                5020 Salzburg<br>
                <br>
                Tel.: 0800/500 810<br>
                Mail: <a style="color: #ffffff;"href="mailto:kundenservice@lidl.at">kundenservice@lidl.at</a><br>
                <br>
                Sitz in Neckarsulm, Registergericht Stuttgart, HRB 105671<br>
                FN: 131556s<br>
                Landesgericht Salzburg<br>
                UID: ATU37893801<br>
                <br>
                Herausgeber und Medienhaber:<br>
                Lidl Österreich GmbH<br>
                Unter der Leiten 11<br>
                5020 Salzburg<br>
            </p>
        @endcomponent
    @endslot
@endcomponent
