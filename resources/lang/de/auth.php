<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Dieses Konto existiert nicht oder die Daten wurden falsch eingegeben.',
    'throttle' => 'Zu viele Login-Versuche. Bitte probiere es in :seconds Sekunden noch einmal.',
    'password' => 'Dieses Konto existiert nicht oder die Daten wurden falsch eingegeben.',
    'email_not_verified' => 'Die angegebene e-Mail Adresse ist wurde noch nicht bestätigt',
    'account_blocked' => 'Dieser Account wurde gesperrt.',

];
