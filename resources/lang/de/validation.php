<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'multiple_of' => 'The :attribute must be a multiple of :value',
    'not_regex' => 'The :attribute format is invalid.',
    'password' => 'Das password ist nicht richtig.',
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'uuid' => 'The :attribute must be a valid UUID.',

    'accepted'             => ':attribute muss akzeptiert werden.',
    'active_url'           => ':attribute ist keine korrekte URL.',
    'after'                => ':attribute muss ein Datum nach dem :date sein.',
    'after_or_equal'       => ':attribute muss ein Datum nach dem oder am :date sein.',
    'alpha'                => ':attribute darf nur Buchstaben enthalten.',
    'alpha_dash'           => ':attribute darf nur Buchstaben, Zahlen und Bindestriche enthalten.',
    'alpha_num'            => ':attribute darf nur Buchstaben und Zahlen enthalten.',
    'array'                => ':attribute muss eine Liste sein.',
    'before'               => ':attribute muss ein Datum vor dem :date sein.',
    'before_or_equal'      => ':attribute muss ein Datum vor dem oder am :date sein.',
    'between'              => [
        'numeric' => ':attribute muss zwischen :min und :max sein.',
        'file'    => ':attribute muss zwischen :min und :max Kilobytes sein.',
        'string'  => ':attribute muss zwischen :min und :max Zeichen sein.',
        'array'   => ':attribute muss zwischen :min und :max Einträge haben.',
    ],
    'boolean'              => ':attribute muss wahr oder falsch sein.',
    'confirmed'            => 'Die :attribute-Bestätigung stimmt nicht überein.',
    'date'                 => ':attribute ist kein gültiges Datum.',
    'date_format'          => ':attribute entspricht nicht dem Format: :format.',
    'different'            => ':attribute und :other müssen verschieden sein.',
    'digits'               => ':attribute muss :digits Ziffern lang sein.',
    'digits_between'       => ':attribute muss zwischen :min und :max Ziffern lang sein.',
    'dimensions'           => ':attribute hat inkorrekte Bild-Dimensionen.',
    'distinct'             => ':attribute hat einen doppelten Wert.',
    'email'                => ':attribute muss eine korrekte E-Mail-Adresse sein.',
    'exists'               => 'Ausgewählte(s) :attribute ist inkorrekt.',
    'file'                 => ':attribute muss eine Datei sein.',
    'filled'               => ':attribute muss ausgefüllt werden.',
    'image'                => ':attribute muss ein Bild sein.',
    'in'                   => 'Ausgewählte(s) :attribute ist inkorrekt.',
    'in_array'             => ':attribute existiert nicht in :other.',
    'integer'              => ':attribute muss eine Ganzzahl sein.',
    'ip'                   => ':attribute muss eine korrekte IP-Adresse sein.',
    'ipv4'                 => ':attribute muss eine korrekte IPv4-Adresse sein.',
    'ipv6'                 => ':attribute muss eine korrekte IPv6-Adresse sein.',
    'json'                 => ':attribute muss ein korrekter JSON-String sein.',
    'max'                  => [
        'numeric' => ':attribute darf nicht größer als :max sein.',
        'file'    => ':attribute darf nicht größer als :max Kilobytes sein.',
        'string'  => ':attribute darf nicht länger als :max Zeichen sein.',
        'array'   => ':attribute darf nicht mehr als :max Einträge enthalten.',
    ],
    'mimes'                => ':attribute muss eine Datei in folgendem Format sein: :values.',
    'mimetypes'            => ':attribute muss eine Datei in folgendem Format sein: :values.',
    'min'                  => [
        'numeric' => ':attribute muss mindestens :min sein.',
        'file'    => ':attribute muss mindestens :min Kilobytes groß sein.',
        'string'  => ':attribute muss mindestens :min Zeichen lang sein.',
        'array'   => ':attribute muss mindestens :min Einträge haben..',
    ],
    'not_in'               => 'Ausgewählte(s) :attribute ist inkorrekt.',
    'numeric'              => ':attribute muss eine Zahl sein.',
    'present'              => ':attribute muss vorhanden sein.',
    'regex'                => 'Das :attribute-Format ist inkorrekt.',
    'required'             => ':attribute wird benötigt.',
    'required_if'          => ':attribute wird benötigt wenn :other einen Wert von :value hat.',
    'required_unless'      => ':attribute wird benötigt außer :other ist in den Werten :values enthalten.',
    'required_with'        => ':attribute wird benötigt wenn :values vorhanden ist.',
    'required_with_all'    => ':attribute wird benötigt wenn :values vorhanden ist.',
    'required_without'     => ':attribute wird benötigt wenn :values nicht vorhanden ist.',
    'required_without_all' => ':attribute wird benötigt wenn keine der Werte :values vorhanden ist.',
    'same'                 => ':attribute und :other müssen gleich sein.',
    'size'                 => [
        'numeric' => ':attribute muss :size groß sein.',
        'file'    => ':attribute muss :size Kilobytes groß sein.',
        'string'  => ':attribute muss :size Zeichen lang sein.',
        'array'   => ':attribute muss :size Einträge enthalten.',
    ],
    'string'               => ':attribute muss Text sein.',
    'timezone'             => ':attribute muss eine korrekte Zeitzone sein.',
    'unique'               => ':attribute wurde bereits verwendet.',
    'uploaded'             => 'Der Upload von :attribute schlug fehl.',
    'url'                  => 'Das :attribute-Format ist inkorrekt.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'tipp.*.home' => [
            'required' => 'Bitte geben ein Ergebniss für Home an.',
            'integer' => 'Bitte gebe eine Zahl an.',
            'gte' => "Bitte gebe eine Zahl größer gleich 0 an."
        ],
        'tipp.*.guest' => [
            'required' => 'Bitte geben ein Ergebniss für Gästemanschaft an.',
            'integer' => 'Bitte gebe eine Zahl an.',
            'gte' => "Bitte gebe eine Zahl größer gleich 0 an."
        ],
        'tipp.*' => [
            'date' => 'Ein Tipp kann nicht mehr getippt werden, da der Abgabetermin überschritten worden ist.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'email' => 'E-Mail Adresse',
        'password' => 'Passwort',
        'password_confirmation' => 'Passwort-Bestätigung',
        'remember' => 'Zugangsdaten merken',
        'name' => 'Name',
        'max_tipp_date' => 'Maximaler Abgabetermin',
        'start_date' => 'Startdatum',
        'end_date' => 'Enddatum',
        'type' => 'Typ',
        'question' => 'Fragen',
        'home' => 'Heimteam',
        'guest' => 'Gastteam',
        'terms' => 'Teilnahmebedingungen',
        'privacy' => 'Datenschutzbestimmungen',
    ],

];
