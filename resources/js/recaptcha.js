const _init_recaptcha = () => {
    document.addEventListener('DOMContentLoaded', () => {
        const _selectors = {
            'input': '[data-controller="recaptcha"]'
        };

        const insertHeadScript = () => {
            if (!window.cake.form.recaptchaInserted) {
                let script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'https://www.google.com/recaptcha/api.js?render=' + googleRecaptchaSiteKey;

                document.getElementsByTagName('head')[0].appendChild(script);
                window.cake.form.recaptchaInserted = true
            }
        }

        const submitForm  = (event, form, input) => {

            if (input.value.length === 0) {
                event.preventDefault();
                grecaptcha.ready(function () {
                    grecaptcha
                        .execute(googleRecaptchaSiteKey, { action: 'submit' })
                        .then(function (token) {
                            input.value = token
                            const submitFormFunction = Object.getPrototypeOf(form).submit;
                            submitFormFunction.call(form)
                        }).catch(function (error) {
                            console.log(error)
                        });
                });
            }
        }

        const recaptchaInputs = document.querySelectorAll(_selectors.input);
        const googleRecaptchaSiteKey = window.cake.recaptchaKey;

        if (recaptchaInputs.length > 0) {
            insertHeadScript();
        }

        recaptchaInputs.forEach( input => {
            input.form.addEventListener('submit', function(event) {
                submitForm(event, this, input)
            });
        });
    });
};

if (typeof window.cake !== "object") {
    window.cake = {};
}
window.cake.recaptcha = _init_recaptcha;

export default (() => {
    _init_recaptcha();
});
