
const _init_select_reload = () => {
    document.addEventListener ('DOMContentLoaded', () => {
        const _selectors = {
            'selectBox': '[data-controller="select/reload"]'
        };

        const selectboxes = document.querySelectorAll(_selectors.selectBox);
        selectboxes.forEach( selectbox => {
            selectbox.onchange = function(ev) {
                window.location = this.value;
            };
        });
    });
};

if (typeof window.cake !== "object") {
    window.cake = {};
}
window.cake.selectReload = _init_select_reload;

export default (() => {
    _init_select_reload();
});
