
const _init_question_create = () => {

        document.addEventListener ('DOMContentLoaded', () => {
            //Get all Tabs
            const forms = document.querySelectorAll('[data-controller="question/create"]');

            let currentType = 0;

            const onChangeType = function(ev) {
                currentType = parseInt(ev.target.value)

                forms.forEach ((form, idx) => {
                    form.querySelectorAll('.type-bonus').forEach((input) => {
                        if (!input.classList.contains('d-none')) input.classList.add('d-none')
                    })
                    form.querySelectorAll('.type-game').forEach((input) => {
                        if (!input.classList.contains('d-none')) input.classList.add('d-none')
                    })

                    setTimeout(function() {
                        if (currentType === 0) {
                            form.querySelectorAll('.type-game').forEach((input) => {
                                if (input.classList.contains('d-none')) input.classList.remove('d-none')
                            })
                        }
                        if (currentType === 1) {
                            form.querySelectorAll('.type-bonus').forEach((input) => {
                                if (input.classList.contains('d-none')) input.classList.remove('d-none')
                            })
                        }
                    }, 10);
                })
            }

            forms.forEach ((form, idx) => {
                //Get all Tab-Buttons of the Tab
                const typeSelector = form.querySelector('[data-controller="question/create/type-select"]');
                typeSelector.addEventListener('change', onChangeType)
                typeSelector.dispatchEvent(new Event('change', { 'bubbles': true }));
            });
        });
};

if (typeof window.cake !== "object") {
    window.cake = {
        question: {}
    };
}
if (typeof window.cake.question !== "object") {
    window.cake.question = {};
}
window.cake.question.create = _init_question_create

export default (() => {
    _init_question_create();
});
