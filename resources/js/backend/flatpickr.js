import flatpickr from "flatpickr";
const German = require("flatpickr/dist/l10n/de.js").default.de;
const _init_flatpickr = () => {

    //document.addEventListener ('DOMContentLoaded', () => {
    flatpickr.localize(German);
    flatpickr(".flatpickr", {
            enableTime: true,
            enableSeconds: true,
            time_24hr: true,
            minuteIncrement: 1,
            secondIncrement: 1,
            dateFormat: "Y-m-d H:i:s",
        });
    //});
};

if (typeof window.cake !== "object") {
    window.cake = {};
}
window.cake.flatpickr = _init_flatpickr

export default (() => {
    _init_flatpickr();
});
