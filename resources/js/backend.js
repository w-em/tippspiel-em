//window._ = require('lodash');
//Import Polyfills and external dependencies
import svg4everybody from "svg4everybody";

//Import CAKE Dependencies as you want (you can simply remove the lines you do not need)
import accordion from "@cake-hub/web-css_framework/js/accordion";
import alert from "@cake-hub/web-css_framework/js/alert";
import form from "@cake-hub/web-css_framework/js/form";
import header from "@cake-hub/web-css_framework/js/header";
import popover from "@cake-hub/web-css_framework/js/popover";
import subnavigation from "@cake-hub/web-css_framework/js/subnavigation";
import "@cake-hub/web-css_framework/js/cookieAlert";
import tab from "@cake-hub/web-css_framework/js/tab";
import themeSlider from "@cake-hub/web-css_framework/js/themeSlider";
import totop from "@cake-hub/web-css_framework/js/toTop";

import selectReload from "./selectReload";
import flatpickr from "./backend/flatpickr";
import question_create from "./backend/game/question_create";

( () => {
    //Run external dependencies
    svg4everybody ();

    //Scripts to load when document-loaded
    // accordion ();
    // alert ();
    form ();
    header ();
    popover ();
    subnavigation ();
    selectReload();
    flatpickr();

    question_create();
})();


/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

//window.axios = require('axios');

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
