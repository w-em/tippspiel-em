//window._ = require('lodash');
//Import Polyfills and external dependencies
import svg4everybody from "svg4everybody";

//Import CAKE Dependencies as you want (you can simply remove the lines you do not need)
// import accordion from "@cake-hub/web-css_framework/js/accordion";
// import alert from "@cake-hub/web-css_framework/js/alert";
import form from "@cake-hub/web-css_framework/js/form";
import header from "@cake-hub/web-css_framework/js/header";
import popover from "@cake-hub/web-css_framework/js/popover";
import subnavigation from "@cake-hub/web-css_framework/js/subnavigation";
// import tab from "@cake-hub/web-css_framework/js/tab";
// import themeSlider from "@cake-hub/web-css_framework/js/themeSlider";
import totop from "@cake-hub/web-css_framework/js/toTop";
import selectReload from "./selectReload";
import recaptcha from "./recaptcha";
import "@cake-hub/web-css_framework/js/cookieAlert";

( () => {
    //Run external dependencies
    svg4everybody ();

    //Scripts to load when document-loaded
    // accordion ();
    // alert ();
    form ();
    header ();
    popover ();
    subnavigation ();
    // tab ();
    /*
    themeSlider.initializeAllSliders (
        {
            fixedWidth: 540,
            responsive: {
                600: {
                    fixedWidth: 540
                }
            }
        },
        true,
        document.querySelector ('*[data-toggle="wins-slider"]')
    );*/
    totop ();
    selectReload();
    recaptcha();
})();
