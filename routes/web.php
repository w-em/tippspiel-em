<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('static.welcome');
})->name('home');

/**
 * static routes
 */
Route::get('/datenschutz', [\App\Http\Controllers\StaticController::class, 'privacyPolicy'])
    ->middleware('web')
    ->name('static.privacy-policy');
Route::get('/cookies', [\App\Http\Controllers\StaticController::class, 'cookies'])
    ->middleware('web')
    ->name('static.cookies');
Route::get('/impressum', [\App\Http\Controllers\StaticController::class, 'impress'])
    ->middleware('web')
    ->name('static.impress');
Route::get('/teilnahmebedingungen', [\App\Http\Controllers\StaticController::class, 'terms'])
    ->middleware('web')
    ->name('static.terms');
Route::get('/gewinne', [\App\Http\Controllers\StaticController::class, 'wins'])
    ->middleware('web')
    ->name('static.wins');
Route::get('/regeln', [\App\Http\Controllers\StaticController::class, 'rules'])
    ->middleware('web')
    ->name('static.rules');
// end

Route::get('/test', [\App\Http\Controllers\TestController::class, 'index'])
    ->middleware('web')
    ->name('test');

Route::get('/dashboard', [\App\Http\Controllers\TippController::class, 'overview'])
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::get('/account', [\App\Http\Controllers\AccountController::class, 'index'])
    ->middleware(['auth', 'verified'])
    ->name('user.account.index');
Route::get('/account/password-aendern', [\App\Http\Controllers\AccountController::class, 'changePasswordIndex'])
    ->middleware(['auth', 'verified'])
    ->name('user.account.password_change.index');
Route::post('/account/password-aendern', [\App\Http\Controllers\AccountController::class, 'changePasswordSave'])
    ->middleware(['auth', 'verified'])
    ->name('user.account.password_change.save');

/**
 * tipp overview
 */
Route::get('/tipp-uebersicht', [\App\Http\Controllers\TippController::class, 'overview'])
    ->middleware(['auth', 'verified'])
    ->name('tipp.overview');
Route::get('/tipp-uebersicht/{id}', [\App\Http\Controllers\TippController::class, 'overview'])
    ->middleware(['auth', 'verified'])
    ->name('tipp.overview.selected');


/**
 * tip the game
 */
Route::get('/tipp-abgabe', [\App\Http\Controllers\TippController::class, 'gameTipp'])
    ->middleware(['auth', 'verified'])
    ->name('tipp.game');

Route::get('/tipp-abgabe/{id}', [\App\Http\Controllers\TippController::class, 'gameTipp'])
    ->middleware(['auth', 'verified'])
    ->name('tipp.game.selected');

Route::post('/tipp-abgabe/{id}', [\App\Http\Controllers\TippController::class, 'gameTippSave'])
    ->middleware(['auth', 'verified'])
    ->name('tipp.game.save');

Route::get('/spielplan', [\App\Http\Controllers\GameScheduleController::class, 'index'])
    ->middleware('web')
    ->name('game_schedule');

Route::get('/highscore', [\App\Http\Controllers\HighscoreController::class, 'index'])
    ->middleware(['web'])
    ->name('highscore');
Route::get('/highscore/{id}', [\App\Http\Controllers\HighscoreController::class, 'index'])
    ->middleware(['web'])
    ->name('highscore.selected');



require __DIR__.'/auth.php';
require __DIR__.'/admin.php';
