<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Admin\AnswerController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\ScheduleController;
use App\Http\Controllers\Admin\WinningRoundController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\PointRuleController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')
    ->name('admin.')
    ->middleware(['auth', 'admin'])
    ->group(function () {

        Route::get('/', [AdminController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('index');

        /**
         * Users
         */
        Route::get('/users', [UserController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('users.index');

        Route::get('/user/create', [UserController::class, 'create'])
            ->middleware(['auth', 'admin'])
            ->name('users.create');
        Route::post('/user/create', [UserController::class, 'store'])
            ->middleware(['auth', 'admin'])
            ->name('users.store');

        Route::get('/user/edit/{id}', [UserController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('users.edit');
        Route::post('/user/edit/{id}', [UserController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('users.update');

        Route::get('/user/delete/{id}', [UserController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('users.delete');
        Route::post('/user/delete/{id}', [UserController::class, 'remove'])
            ->middleware(['auth', 'admin'])
            ->name('users.remove');

        /**
         * games
         */
        Route::get('/games', [AdminController::class, 'gamesIndex'])
            ->middleware(['auth', 'admin'])
            ->name('games');

        /**
         * schedules / Spieltage
         */
        Route::get('/schedules', [ScheduleController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.index');

        Route::get('/schedule/create', [ScheduleController::class, 'create'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.create');
        Route::post('/schedule/create', [ScheduleController::class, 'store'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.store');

        Route::get('/schedule/edit/{id}', [ScheduleController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.edit');
        Route::post('/schedule/edit/{id}', [ScheduleController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.update');

        Route::get('/schedule/delete/{id}', [ScheduleController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.delete');

        Route::post('/schedule/delete/{id}', [ScheduleController::class, 'remove'])
            ->middleware(['auth', 'admin'])
            ->name('schedules.remove');

        /**
         * teams
         */
        Route::get('/teams', [TeamController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('teams.index');

        Route::get('/team/create', [TeamController::class, 'create'])
            ->middleware(['auth', 'admin'])
            ->name('teams.create');
        Route::post('/team/create', [TeamController::class, 'store'])
            ->middleware(['auth', 'admin'])
            ->name('teams.store');

        Route::get('/team/edit/{id}', [TeamController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('teams.edit');
        Route::post('/teams/edit/{id}', [TeamController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('teams.update');

        Route::get('/team/delete/{id}', [TeamController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('teams.delete');

        /**
         * Questions
         */
        Route::get('/questions', [QuestionController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('questions.index');

        Route::get('/question/create', [QuestionController::class, 'create'])
            ->middleware(['auth', 'admin'])
            ->name('questions.create');
        Route::post('/question/create', [QuestionController::class, 'store'])
            ->middleware(['auth', 'admin'])
            ->name('questions.store');

        Route::get('/question/edit/{id}', [QuestionController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('questions.edit');
        Route::post('/question/edit/{id}', [QuestionController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('questions.update');

        Route::get('/question/delete/{id}', [QuestionController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('questions.delete');
        Route::post('/question/delete/{id}', [QuestionController::class, 'remove'])
            ->middleware(['auth', 'admin'])
            ->name('questions.remove');
        /**
         * Answers
         */
        Route::get('/answers', [AnswerController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('answers.index');

        Route::get('/answer/edit/{id}', [AnswerController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('answers.edit');
        Route::post('/answer/edit/{id}', [AnswerController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('answers.update');

        Route::get('/answer/delete/{id}', [AnswerController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('answers.delete');

        /**
         * winning rounds
         */
        Route::get('/winning_rounds', [WinningRoundController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.index');

        Route::get('/winning_round/create', [WinningRoundController::class, 'create'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.create');
        Route::post('/winning_round/create', [WinningRoundController::class, 'store'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.store');

        Route::get('/winning_round/edit/{id}', [WinningRoundController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.edit');
        Route::post('/winning_round/edit/{id}', [WinningRoundController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.edit.update');

        Route::get('/winning_round/delete/{id}', [WinningRoundController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.delete');

        Route::post('/winning_round/delete/{id}', [WinningRoundController::class, 'remove'])
            ->middleware(['auth', 'admin'])
            ->name('winning_rounds.remove');


        /**
         * Point rules
         */
        Route::get('/pointRules', [PointRuleController::class, 'index'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.index');

        Route::get('/pointRules/create', [PointRuleController::class, 'create'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.create');
        Route::post('/pointRules/create', [PointRuleController::class, 'store'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.store');

        Route::get('/pointRules/edit/{id}', [PointRuleController::class, 'edit'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.edit');
        Route::post('/pointRules/edit/{id}', [PointRuleController::class, 'update'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.edit.update');

        Route::get('/pointRules/delete/{id}', [PointRuleController::class, 'delete'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.delete');

        Route::post('/pointRules/delete/{id}', [PointRuleController::class, 'remove'])
            ->middleware(['auth', 'admin'])
            ->name('point_rules.remove');
});



